OCAML_VERSION=4.11.0
OCAMLFORMAT_VERSION=0.15.0

SCRIPT_DIR="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
SRC_DIR="$(dirname "$script_dir")"

if [ ! -d "${SRC_DIR}/_opam" ] ; then
   opam switch create ./ ${OCAML_VERSION} --empty
   opam depext conf-gmp conf-pkg-config
fi

opam install . -y
opam install ocamlformat.${OCAMLFORMAT_VERSION} merlin
