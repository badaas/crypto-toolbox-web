open Js_of_ocaml

type t =
  { width : int option;
    height : int option;
    title : string;
    data_class : string list;
    btn_close : bool;
    btn_min : bool;
    btn_max : bool;
    id : string
  }

let create ?width ?height ?(btn_close = true) ?(btn_max = true)
    ?(btn_min = true) ?(data_class = []) title id =
  { width; height; title; data_class; btn_close; btn_min; btn_max; id }

let generate_html t =
  let n = Dom_html.createDiv Dom_html.document in
  n##.id := Js.string t.id ;
  n##setAttribute (Js.string "data-role") (Js.string "window") ;
  n##setAttribute
    (Js.string "data-class")
    (Js.string (String.concat " " t.data_class)) ;
  n##setAttribute (Js.string "data-title") (Js.string t.title) ;
  n##setAttribute
    (Js.string "data-btn-close")
    (Js.string (string_of_bool t.btn_close)) ;
  n##setAttribute (Js.string "data-title") (Js.string t.title) ;
  n##setAttribute
    (Js.string "data-btn-min")
    (Js.string (string_of_bool t.btn_min)) ;
  n##setAttribute
    (Js.string "data-btn-max")
    (Js.string (string_of_bool t.btn_max)) ;
  n
