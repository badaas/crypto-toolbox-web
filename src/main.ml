open Js_of_ocaml_lwt
open Js_of_ocaml

let window_area =
  let div = Dom_html.createDiv Dom_html.document in
  div##setAttribute (Js.string "class") (Js.string "window-area") ;
  div

type window_affine_edwards =
  { window : Dom_html.divElement Js.t;
    input_u : Dom_html.inputElement Js.t;
    input_v : Dom_html.inputElement Js.t;
    button : Dom_html.buttonElement Js.t
  }

let create_input id size =
  let input = Dom_html.createInput Dom_html.document in
  input##setAttribute (Js.string "id") (Js.string id) ;
  input##setAttribute (Js.string "size") (Js.string size) ;
  input

let create_window_for_affine_edwards (module Ec : Ec_sig.AffineEdwardsT)
    id_prefix title =
  let window =
    Metro_ui.Window.create
      ~btn_close:true
      ~btn_min:false
      ~btn_max:false
      ~data_class:["p-2"]
      title
      (Printf.sprintf "-window-%s" id_prefix)
  in
  let window = Metro_ui.Window.generate_html window in
  let button = Dom_html.createButton Dom_html.document in
  button##setAttribute (Js.string "class") (Js.string "button") ;
  button##setAttribute
    (Js.string "id")
    (Js.string (Printf.sprintf "%s-button-generate" id_prefix)) ;
  let span = Dom_html.createSpan Dom_html.document in
  span##setAttribute (Js.string "class") (Js.string "mif-refresh mif-lg") ;
  Dom.appendChild button span ;
  let input_u = create_input ((Printf.sprintf "%s-random-u") id_prefix) "64" in
  let input_v = create_input ((Printf.sprintf "%s-random-u") id_prefix) "64" in
  Dom.appendChild window button ;
  Dom.appendChild window input_u ;
  Dom.appendChild window input_v ;
  Lwt_js_events.(
    async (fun () ->
        clicks button (fun _ev _thread ->
            (* let jubjub_input_u =
             *   Jsoo_lib.get_input_by_id_exn "jubjub-random-u"
             * in
             * let jubjub_input_v =
             *   Jsoo_lib.get_input_by_id_exn "jubjub-random-v"
             * in *)
            let r = Ec.random () in
            let u = Ec.get_u_coordinate r in
            let v = Ec.get_v_coordinate r in
            Jsoo_lib.Input.set_content
              input_u
              (Printf.sprintf
                 "0x%s"
                 (Hex.show (Hex.of_bytes (Ec.BaseField.to_bytes u)))) ;
            Jsoo_lib.Input.set_content
              input_v
              (Printf.sprintf
                 "0x%s"
                 (Hex.show (Hex.of_bytes (Ec.BaseField.to_bytes v)))) ;
            Lwt.return ()))) ;
  { window; input_u; input_v; button }

let onload _ =
  (* main container, element must be added and removed from this one *)
  let desktop = Jsoo_lib.get_div_by_id_exn "desktop" in
  let jubjub_new =
    create_window_for_affine_edwards
      (module Ec_jubjub.Affine : Ec_sig.AffineEdwardsT)
      "jubjub"
      "Jubjub"
  in
  let babyjubjub =
    create_window_for_affine_edwards
      (module Ec_babyjubjub.Affine : Ec_sig.AffineEdwardsT)
      "babyjubjub"
      "BabyJubjub"
  in
  let babyjubjub_reduced =
    create_window_for_affine_edwards
      (module Ec_babyjubjub.Affine : Ec_sig.AffineEdwardsT)
      "babyjubjub-reduced"
      "BabyJubjub Reduced"
  in
  (* Insert windows *)
  Dom.appendChild window_area jubjub_new.window ;
  Dom.appendChild window_area babyjubjub.window ;
  Dom.appendChild window_area babyjubjub_reduced.window ;
  Dom.insertBefore
    desktop
    window_area
    (Js.Opt.option @@ Jsoo_lib.get_div_by_id_opt "task-bar") ;
  Js._true

let _ = Jsoo_lib.onload onload
